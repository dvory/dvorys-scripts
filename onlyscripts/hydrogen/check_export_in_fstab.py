#!/usr/local/bin/python

import os
import re

mul = [""]

def check_export_in_node(node_name):
        full_command = "ssh -q -x " + node_name + " df"
        command_output = os.popen(full_command)
        output = command_output.read()
#       print "connected to " + node_name
        # if the /export dir was not mounted - mount it
        if output.find("/export") == -1:
                command = "ssh -q -x " + node_name + " mount -a"
#               print "activate ,mount -a"

nodes = os.popen("cat /etc/hosts").readlines()
for node in nodes:
        compute_node = re.search(r"compute-0-\d*", node, re.I)
        if compute_node and compute_node.group() not in mul:
                check_export_in_node(compute_node.group())

