#!/usr/local/bin/python
#
# Wonderfull site for plotting:
# https://pythonprogramming.net/matplotlib-python-3-basics-tutorial/
import os
import utils

public_queues = ["short", "inf", "parallel", "bigmem", "hugemem"]

# The below is copied from /opt/torque/server_priv/nodes
path_of_nodes_file = "/powerapps/data/hpc-load/nodes"
HISTORY_N_TIME_TO_SAVE = 90	 #more than a month has passed, keep only last month

# num of elements is +1 since array starts from 0, and the index (in power) is the compute node number
# so number of elements is actually 196
#GLOBAL_POWER_ELEMENTS=196

GLOBAL_POWER_ELEMENTS = utils.Get_number_of_nodes()

hostname=os.popen("hostname").read()
if hostname.find("power") != -1 or hostname.find("admin") != -1:
        host = "power"
	current_path = "/powerapps/scripts/hpc-load"
else:
        host = "lecs2"
	current_path = "/root/dvory/Python/hpc-load"

path_of_saved_file = current_path + "/data/nodes_and_ppns.p" # This path is shared between power2 and power6
path_of_pbs_pro_saved_file = current_path + "/data/pbs_pro_nodes_and_ppns.p" # This path is shared between power2 and power6

# --------------------------------------------------------------
#      Extracting the index from the compute node name
# This is static method, not relies on a compute node class
# --------------------------------------------------------------
def Get_index(compute_name):
        if compute_name.find("compute-0-") == -1:
                return -1
        return int(compute_name[len("compute-0-"):])

# ------------------------------------------------------
#      compute node class
#
# Contains the following:
#	name - 			Compute node name
#	public_queues - 	List of my public queues
#	private_queues - 	List of my provate queues
#	exist - 		flag notes whether this node exists(or empty index)
#	np -			Number of the cores in this node
#	cpu_load -		How much loaded this node
# ------------------------------------------------------
class Compute_node:
	def __init__(self, is_exist, tokenized_line):
		self.private_queues = []
		self.public_queues = []
		self.exist = is_exist
		self.ok = True
		self.cpu_load = 0	#raw value accepted from uptime, not normalized
		self.normalized_cpu_load = 0
		self.average_load = 0	#raw value, not normalized to cpus
		self.normalized_average_load = 0
		self.normalized_load = 0
		self.history_list = []
		self.queues =""
		self.nElements = 0
		if is_exist == True:
			self.name = tokenized_line[0]
			np_string = tokenized_line[1]		# the syntax is: np=
			self.np = int(np_string[3:])		# So need to save only the number afterwards
		else:
			self.name ="-1"
			np_string = ""
			self.np = 1
		for i in range (2, len(tokenized_line)):
			self.queues += " " + tokenized_line[i]
			if tokenized_line[i] in public_queues:
				self.public_queues.append(tokenized_line[i])
			else:
				self.private_queues.append(tokenized_line[i])
	def Set_num_elements(self, num_elements):
		self.nElements = num_elements

	def Get_num_elements(self):
		return self.nElements

	def Fill_my_name(self, name):
		self.name = name
		self.cpu_load = 0

	def Fill_nCores(self, nCores):
		self.np = int(nCores)
	def Fill_load_data(self, load_raw):
		self.cpu_load = float(load_raw)
		self.normalized_load = float(((self.cpu_load * 100)) / float(self.np))
	def Set_is_exist(self, flag):
		self.exist = flag
		self.cpu_load = 0
	def Set_is_ok(self, flag):
		self.ok = flag
	def Get_is_ok(self):
		return self.ok
	def Add_queues(self,queues):
		self.queues += queues
	def Get_my_queues(self):
		return self.queues
	def Update_history(self, load_raw):
		self.history_list.append(float(load_raw))
		if len (self.history_list) >= HISTORY_N_TIME_TO_SAVE:
			del self.history_list[0]
		average = 0
		for index in range (0, len(self.history_list)):
			average += float(self.history_list[index])
		self.average_load = float(average)/len(self.history_list)
		self.normalized_load = float(((self.cpu_load * 100)) / float(self.np))
		self.normalized_average_load = float(average)/len(self.history_list)/float(self.np)
	def Get_my_percentage_load(self, is_average):
		load = float(100)
		if self.exist == True:
			if is_average == True:
				load = float(((self.average_load * 100)) / float(self.np))
				self.normalized_average_load = load
			else:
				load = float(((self.cpu_load * 100)) / float(self.np))
				self.normalized_load = load
			if load > float(100):
				return float(100)
		return load
	def Get_my_average():
		return self.average_load
	def Print_me(self):
		print self.exist
		if self.exist == True:
			print self.name
			print "N processors: " + str(self.np)
			print self.public_queues
			print self.private_queues
			print "Load is :" + str(self.cpu_load)
			print self.history_list
			print self.cpu_load
			print self.average_load
			print self.normalized_load
			print self.normalized_average_load
			print self.ok
			print "queues are: " + self.Get_my_queues()
		else:
			print "\nMissing......................\n"
	def My_name(self):
		return self.name

	def My_last_name(self):
		return self.name[8:]

	def My_color(self, average):
		if self.exist == True:
			if average == True:
				if self.normalized_average_load > float(100):
					return 'indigo'
				return 'g'
			else:
				if self.normalized_load > float(100):
					return 'indigo'
				return 'g'
		else:
			return 'red'
	def IsExist(self):
		return self.exist
	def My_ppn(self):
		return int(self.np)
	def My_index(self):
		return Get_index(self.name)
