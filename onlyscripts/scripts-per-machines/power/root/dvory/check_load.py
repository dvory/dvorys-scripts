#!/usr/local/bin/python

import os
import sys
import time
import re
import utils

users_got_emails=[]

#utils.send_email("dvory@tauex.tau.ac.il", "checking...", "lalala")
def uptime_measure():
# tokenized_line[9] - last minute
# tokenized_line[10]- last 5 minutes
# tokenized_line[11] - last 15 minutes
	uptime_output = os.popen("uptime").read()
	tokenized_line = uptime_output.split()
	return tokenized_line[9]

def find_and_send_email_address(username, active_process):
	if username.isdigit() == True:
		ldap_command="ldapsearch -x -LLL uidNumber=" + username
	else:
		ldap_command = "ldapsearch -x -LLL uid=" + username
	ldap_output = os.popen(ldap_command).read()
	mail_address = re.search(r"mail:.*", ldap_output, re.I)
	username = re.search(r"uid:.*", ldap_output, re.I)
	extracted_username=(username.group())[5:13] #extracting 8 characters, since this is the name in 'w' output
	if mail_address and mail_address.group() not in users_got_emails:
		users_got_emails.append(mail_address.group())
		extracted_email_address= (mail_address.group())[6:]
		email_list = extracted_email_address + ",dvory@tauex.tau.ac.il,danny@post.tau.ac.il"
		utils.send_email(email_list, "Please kill your job on power head node", active_process)
		# write to the user's terminals
		message= "Please kill immediately your process " + active_process
		w_output = os.popen("w").readlines()
		for line in w_output:
			if line.find(username) == 0:
				tokenized_line = line.split()
				message_for_user = "echo \"" + message + "\" > /dev/" + tokenized_line[1]
				os.system(message_for_user)

def send_email(username, active_process):
	if username == "root":
		return
        if active_process.find("tcsh") > -1:
                # did not find a normal process
                return
	find_and_send_email_address(username, active_process)

def inspect_usage_line(line):
	tokenized_line = line.split()
        if float(tokenized_line[0]) >= 0.7:
		send_email(tokenized_line[1], tokenized_line[3])
	return ""
count = 0
heavy_load_count = 0

while (True) :
	count += 1
        if count >= 3:
                break
	load_in_last_minute = uptime_measure()
	float_num = float(load_in_last_minute[:-1])
	if float_num < 2:
        	continue
	heavy_load_count+=1
        time.sleep(20)
	
if heavy_load_count < 2:	# false alarm
	sys.exit()

# If we got here:  load is >= 2
email_body = os.popen("tcsh /root/dvory/head_node_load-vladi /var/log/a 2> /dev/null").read()

# send email to our group
subject = "Power load is " + load_in_last_minute
utils.send_email("dvory@tauex.tau.ac.il,danny@post.tau.ac.il", subject, email_body)

# Send email to the person with the high cpu consumption
count = 0
for line in os.popen("tcsh /root/dvory/head_node_load-vladi /var/log/a 2> /dev/null").readlines():
	if count >= 4:	#after the prefix comes the cpu usage lines
		# Check who is using heavy usage
		print inspect_usage_line(line)
	count += 1
	if count > 10:
		break
