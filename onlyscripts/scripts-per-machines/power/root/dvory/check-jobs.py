#!/usr/local/bin/python
import os
import re
import threading
import subprocess

mul = ["compute-0-43", "compute-0-104", "compute-0-164"]

def Display_queue_menu(name):
        qmgr_command_line = "\"list queue " + name + "\""
        full_command_line = "qmgr -c " + qmgr_command_line
        os.system(full_command_line)

def Get_compute_nodes_for_queue(q_name):
        acl_hosts=[]
        command_line = "qmgr -c \"list queue " + q_name + "\""
        output_line = os.popen(command_line).read()
        low_index=output_line.find("acl_hosts = ")
	if low_index == -1:
		#This queue does not exist
		return None
        high_index = output_line.find("resources")
        all_hosts = output_line[low_index+12:high_index]
        acl_hosts = re.findall(r"compute-\d*-\d*",all_hosts,re.I)
	return acl_hosts

def Display_compute_nodes(q_name):
        print Get_compute_nodes_for_queue(q_name)

def Display_queues():
        output_line = os.popen("qstat -q").read()
        print output_line

def Display_main_menu():
	chosen_number = raw_input("\nPlease Enter your choice:\n\n1. Chose a queue\n2. Show all queues\n3. Send command to all compute nodes\n4. Check jobs (takes a few minutes)\n5. Exit script\n\nYour choice: ")
	num = re.search(r"[1-5]", chosen_number, re.I)
	if num:
		return int(num.group())
	else:
		return 0

def Display_main_queue_menu():
	return int(raw_input("\n\nPlease choose an action:\n\n1. Display the queue\n2. Display compute nodes for the queue\n3. Run a command on queue compute node\n4. Run commands on all compute nodes for this queue\n5. Go to previous menu\n\nYour choice: "))

def Run_command_in_node(node_name, command):
        full_command = "ssh -q -x " + node_name + " \"hostname && " + command + "\""
        command_output = os.popen(full_command)
	output = command_output.read()
	print output
	returned_status = command_output.close()
	if returned_status:	# checking the return status of the command
		write_to_file = node_name + output
        	os.system("echo " + write_to_file + " > " +  path_failed_nodes[:-1] + "/" + node_name)

def Check_jobs_using_1_core():
	user_list =[]
	# Filtering the list in order to get only the job number which is the first column:
	full_command = "qstat | tail -n +3 | cut -d \".\" -f \"1\""
	command_output = os.popen(full_command).readlines()
	for job_number in command_output:
		# Redirecting error to /dev/null
		checkjob_command = "checkjob -v " + job_number[:-1] + " 2> /dev/null"
		checkjob_output = os.popen(checkjob_command).read()
		tasks_field = re.search(r"Total Tasks: \d*", checkjob_output, re.I)
		if tasks_field and tasks_field.group():
			if tasks_field.group() == "Total Tasks: 1":
				utilized_procs = re.search(r"Utilized Resources Per Task: *PROCS: \d*", checkjob_output, re.I)
				if utilized_procs and utilized_procs.group():
					if utilized_procs.group()[len(utilized_procs.group())-2:len(utilized_procs.group())] == " 1":
						# Found a job which uses only 1 core
#						print "Found job #" + job_number
						user_details = re.search(r"Creds:.*user:.* g", checkjob_output, re.I)
						user_line = user_details.group()[:-1].split(':')
						if user_line[2] not in user_list:
							user_list.append(user_line[2])
	print user_list

"""--------------------------------------------------------------------------------
Main function
---------------------------------------------------------------------------------"""

while True:
        choice = Display_main_menu()
        if choice == 1:
                queue_name = raw_input("please enter queue name\nName: ")
                while True:
                        action_chosen = Display_main_queue_menu()
                        if action_chosen == 1:
                                Display_queue_menu(queue_name)
                        elif action_chosen == 2:
                                Display_compute_nodes(queue_name)
                        elif action_chosen == 3:
                                compute_node_name = raw_input("\nPlease enter compute node number> compute-0-")
				command = raw_input("please enter your command> ")
				Run_command_in_node("compute-0-" + compute_node_name, command)
				raw_input("")
			elif action_chosen == 4:
				while True:
                                	nodes = Get_compute_nodes_for_queue(queue_name)
					if nodes == None:
						break
                                	command = raw_input("please enter your command> ")
                                	for node in nodes:
		                               	print node + ":"
                		               	full_command = "ssh -q -x " + node + " " + command
                                		os.system(full_command)
					again = raw_input("Place another command to compute nodes?\n y/n > ")
					if again == "n":
						break
                        elif action_chosen == 5:
                                break   
                        else:
                                print "\n\nIlegal choice !!!!!\n\n\nPlease choose 1..5"
        elif choice == 2:
                Display_queues()
	elif choice == 3:
	        nodes = os.popen("cat /etc/hosts").readlines()
                command = raw_input("please enter your command> ")
		path_failed_nodes = "/tmp/" + os.popen("date +%H%M").read()
		os.system("mkdir " + path_failed_nodes)
		used_nodes=[]
		threads=[]
                for node in nodes:
			compute_node = re.search(r"compute-0-\d*", node, re.I)
                        if (compute_node) and (compute_node.group() not in mul) and (compute_node.group() not in used_nodes):
				used_nodes.append(compute_node.group())
				new_thread=threading.Thread(target=Run_command_in_node, args=(compute_node.group(), command,))
				threads.append(new_thread)
				new_thread.start()
		for cur_thread in threads:
			cur_thread.join(10)
		# Need to count the number of files, and remove the whole directory
		print "The following compute nodes returned error status:\n"
		os.system("cat " + path_failed_nodes[:-1] + "/*")
		os.system("\\rm -rf " + path_failed_nodes[:-1])
	elif choice == 4:
		Check_jobs_using_1_core()
        elif choice == 5:
		break
        else:
                print "\n\n\nIlegal choice !!!\n\n\n"

