#!/usr/bin/env python
#/usr/local/python-anaconda-2.7/bin/python

import os
import datetime
import time
import re
import pickle
import nodes_db
import sys


def Get_SGE_nodes_for_queue(q_name):
        acl_hosts = []
        command_line = "qconf -sq " + q_name + " 2> /dev/null"
        output_line = os.popen(command_line).read()
        low_index=output_line.find("hostlist")
        if low_index == -1:
                #This queue does not exist
                return None
        high_index = output_line.find("seq_no")
        all_hosts = output_line[low_index+9:high_index]
	acl_hosts += re.findall(r"compute-\d*-\d*",all_hosts,re.I)
	if acl_hosts:
		return acl_hosts
	#sometimes there are several groups of hosts:
	tokenized_line = all_hosts.split()
	for i in range (0, len(tokenized_line)):
		command_line = "qconf -shgrp " + tokenized_line[i] + " 2> /dev/null"
		all_hosts = os.popen(command_line).read()
		acl_hosts += re.findall(r"compute-\d*-\d*",all_hosts,re.I)
	return acl_hosts
