#!/usr/bin/python

import os
import os.path


def create_home_dir (path, user, ldap_group):
        os.system("mkdir " + path)
        command_chown = "chown " + user + ":" + ldap_group + " " + path
        os.system(command_chown)

def add_user_to_queue(ldap_user, ldap_group):
        file_name = "/tmp/" + os.popen("date +%H%M").read()
        file_name = file_name[:-1]
        file_Mu_desc = open(file_name, 'w')
        group_in_queue = os.popen("qconf -su " + ldap_group)
        field_to_search = "entries "
        for line in group_in_queue.readlines():
                if line.find(field_to_search) != -1:
                        # line with names has been found - add the new name
                        new_line = field_to_search + ldap_user + "," + line[len(field_to_search):]
                        file_Mu_desc.write(new_line)
			print "lalala"
                else:
                        file_Mu_desc.write(line)
			print "just copy"
        file_Mu_desc.close()
        os.system("qconf -Mu " + file_name)

def add_bio_user(ldap_user, ldap_group, bio_user, bio_group):
        dir_path = "/groups/" + bio_group
        if (os.path.exists(dir_path)):
                new_user_path = dir_path + "//" + bio_user
                if (os.path.exists(new_user_path)):
                        return -1
                create_home_dir(new_user_path, bio_user, ldap_group)
                add_user_to_queue(ldap_user,ldap_group)

