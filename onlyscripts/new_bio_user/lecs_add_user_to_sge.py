#!/usr/bin/python

import sys
import os
from utils import *

params = list(sys.argv)

if len(sys.argv) == 5:
        ldap_username = params[1]
        ldap_group = params[2]
        bio_username = params[3]
        bio_group = params[4]

        newbio_command = "/root/newbiouser.d " + ldap_username + ldap_group + bio_username + bio_group

        status = os.system(newbio_command)
        #create user locally:
        add_bio_user(ldap_username, ldap_group, bio_username, bio_group)

        #create user in jekyl:
        command_to_jekyl = "ssh -x jekyl /root/dvory/Python/add_remote_bio_user " + ldap_username + " " + ldap_group + " " + bio_username + " " + bio_group
        os.system(command_to_jekyl)

""" status is useless-  need another way to check if success
if status != 0:
        print "fail"
else:
        print "success"
"""


