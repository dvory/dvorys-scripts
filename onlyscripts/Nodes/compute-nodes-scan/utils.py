#!/usr/bin/python

import os
import smtplib

from email.mime.text import MIMEText
#from email.MIMEMultipart import MIMEMultipart
from email.mime.multipart import MIMEMultipart

#-----------------------------------------------------------------------------------
#       General utility to send email
#-----------------------------------------------------------------------------------
def send_email(to_list, subject, email_body):
        mail_msg = MIMEMultipart()
        mail_msg['Subject'] = subject
        from_email = os.popen("hostname").read()
        mail_msg['From'] = from_email[:-1]
        mail_msg['To'] = to_list
        mail_msg.attach(MIMEText(email_body,'plain'))

        s = smtplib.SMTP('post.tau.ac.il')
        s.sendmail(mail_msg['From'], mail_msg['To'].split(",") , mail_msg.as_string())


def send_email_with_cc(to_list, cc_list, subject, email_body):
        mail_msg = MIMEMultipart()
        mail_msg['Subject'] = subject
        from_email = os.popen("hostname").read()
        mail_msg['From'] = from_email[:-1]
        mail_msg['To'] = to_list
        mail_msg['Cc'] = cc_list
        mail_msg.attach(MIMEText(email_body,'plain'))

        s = smtplib.SMTP('post.tau.ac.il')
        s.sendmail(mail_msg['From'], mail_msg['To'].split(",") , mail_msg.as_string())

#-----------------------------------------------------------------------------------
#       General utility to log messages
#-----------------------------------------------------------------------------------
def log_message(message):

        #Adding time to the log file
        current_time = os.popen("date +%F:%H%M").read()
        os.system("echo " + current_time[:-1] + " - " + message + " >> /var/log/scan_log.log")

