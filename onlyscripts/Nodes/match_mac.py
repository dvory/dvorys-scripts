#!/usr/bin/env python

""" accept 2 files (power8: /root/dvory/nodes, /root/dvory/list) - which contain mac addresses
in different formats
nodes (all nodes and their mac addresses in power8):
compute-0-190
cpus: 16
             total       used       free     shared    buffers     cached
Mem:      66000044    1562560   64437484          0     187248     387732
eth0      Link encap:Ethernet  HWaddr 00:25:90:FE:06:6C  
          inet addr: 132.66.114.148  Bcast:132.66.115.255  Mask:255.255.254.0

compute-0-191
cpus: 20
             total       used       free     shared    buffers     cached
Mem:     132029692   45948788   86080904          0     189956   43204972
eth0      Link encap:Ethernet  HWaddr 70:10:6F:4A:35:68  
          inet addr: 132.66.114.152  Bcast:132.66.115.255  Mask:255.255.254.0
and list (of all nodes connected to a specific switch):
   1    001e.6724.6f3b    DYNAMIC     Gi1/0/20
   1    001e.6724.6f3d    DYNAMIC     Gi1/0/20
   1    001e.6724.71ab    DYNAMIC     Gi1/0/41

And see if there is a match between mac addresses - and if so - indicate to which node it belongs
"""

import os
import re

mac_list = []

def Find_match(mac_add, mac_list):
	for mac in mac_list:
		mac_upper = mac.upper()
#		mac_add_upper = mac_add.upper()
		list_mac = list(mac_add)
		# Modifying the format (XX:XX:XX:XX:XX:XX: --> XXXX.XXXX.XXXX, in order to compare mac address with same formats
		list_mac[2] = list_mac[3]
		list_mac[3] = list_mac[4]
		list_mac[4] = '.'
                list_mac[5] = list_mac[6]
                list_mac[6] = list_mac[7]
                list_mac[7] = list_mac[9]
                list_mac[8] = list_mac[10]
		list_mac[9] = '.'
                list_mac[10] = list_mac[12]
                list_mac[11] = list_mac[13]
                list_mac[12] = list_mac[15]
                list_mac[13] = list_mac[16]
		mac_add_upper = ''.join(list_mac)
		mac_add_upper = mac_add_upper[:14]
#		print "---" + mac_add_upper + "---"
#		print "+++" + mac_upper + "+++"
		if mac_add_upper == mac_upper:
			return True
	return False

file_macs = os.popen("cat /root/dvory/list").readlines()
for line in file_macs:
	mac = re.search(r"[0-9a-fA-F]{4}\.[0-9a-fA-F]{4}\.[0-9a-fA-F]{4}", line, re.I)
	if mac:
		mac_list.append(mac.group())
#print mac_list

file_nodes = os.popen("cat /root/dvory/nodes").readlines()
found_node = False
for line in file_nodes:
	if found_node == True:
		mac_address = re.search(r"([0-9a-fA-F]{2}:){5}[0-9a-fA-F]{2}", line, re.I)
		if mac_address:
#			print mac_address.group()
			found_node = False
			if Find_match(mac_address.group(), mac_list) == True:
				print "matched: " + compute.group()
	else:
		compute = re.search(r"compute-0-\d*", line, re.I)
		if compute:
			found_node = True	
