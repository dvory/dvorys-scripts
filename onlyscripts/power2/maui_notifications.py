 not work in dvory machine - the path of python is incorrect
# Activate a command for several host - to be accepted as an input file
#!/usr/local/bin/python

# does not work in dvory machine - the path of python is incorrect
# Activate a command for several host - to be accepted as an input file

import sys
import os
sys.path.insert(0, '/export/dvory/power2-scripts')
import utils

params = list(sys.argv)

if len(sys.argv) == 2:
        notification_from_maui = params[1]
else:
        notification_from_maui = "Number of parameters: " + str(len(sys.argv))
if notification_from_maui.find("JOBCORRUPTION") != -1:
        # If this particular error is found - happens all the time- then send an email
        # just if it is the first time
        if os.path.isfile("/tmp/dvory-maui-corruption-notification") == False:
                # the file does not exist, probably maui just restarted - 
                # send notification and create the file
                os.system("touch /tmp/dvory-maui-corruption-notification")
                utils.send_email("dvory@tauex.tau.ac.il", "maui start notification", notification_from_maui)
elif notification_from_maui.find("JOBWCVIOLATION") != -1:
        # If this clock limit violation happened for the first time - send an email
        if os.path.isfile("/tmp/dvory-maui-clock-violation") == False:
                # the file does not exist,
                # send notification and create the file
                os.system("touch /tmp/dvory-maui-clock-violation")
                utils.send_email("dvory@tauex.tau.ac.il", "maui clock notification", notification_from_maui)
else:
        if os.path.isfile("/tmp/dvory-maui-notification") == False:
                # the file does not exist - 
                # send notification and create the file
                os.system("touch /tmp/dvory-maui-notification")
                utils.send_email("dvory@tauex.tau.ac.il", "maui error notification", notification_from_maui)

