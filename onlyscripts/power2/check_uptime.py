#!/usr/local/python-anaconda-2.7/bin/python

"""
 This script is ran by power2, in the hourly cron.
 It checks for servers, which their uptime is less than an hour.
 If found such a compute node - it runs Pini's script : fix_fstab_gw, which:
 checks whether the /etc/fstab file has been changed - and if so - copies
 the correct file to /etc/fstab
"""
import os
import re
import utils

mul = ["compute-0-43", "compute-0-104", "compute-0-164"]

def check_uptime_in_node(node_name):
        full_command = "ssh -q -x " + node_name + " uptime"
        command_output = os.popen(full_command).read()
        # the below condition:
        # did not find 'day' string in the uptime output
        # and - found 'min' string in the uptime output
        # ----> in this case it means that there was a reset in the last hour
        if command_output.find("day") == -1 and command_output.find("min") != -1:
                utils.send_email("dvory@post.tau.ac.il,danny@post.tau.ac.il,piniko@post.tau.ac.il,chernogorsky@gmail.com", "--auto msg-- Please check me", "compute node reset")

                #Activate Pini's script of copying the fstab  - no need - puppet does this
                #os.system("ssh -q -x " + node_name + " source /export/dvory/fix_fstab_gw >& /dev/null")

nodes = os.popen("cat /etc/hosts").readlines()
used_nodes=[]
for node in nodes:
        compute_node = re.search(r"compute-0-\d*", node, re.I)
        if compute_node and compute_node.group() not in mul and compute_node.group() not in used_nodes:
                used_nodes.append(compute_node.group())
                check_uptime_in_node(compute_node.group())

