#!/usr/bin/python

import os
import smtplib
from os.path import basename

# for power
from email.MIMEText import MIMEText
from email.MIMEMultipart import MIMEMultipart
from email.mime.application import MIMEApplication
#-----------------------------------------------------------------------------------
#       General utility to send email
#-----------------------------------------------------------------------------------
def send_email(to_list, subject, email_body,files=None):
        mail_msg = MIMEMultipart()
        mail_msg['Subject'] = subject
        from_sender = os.popen("hostname").read()
        mail_msg['From'] = from_sender[:-1]
        mail_msg['To'] = to_list
        mail_msg.attach(MIMEText(email_body,'plain'))

#	for f in files or []:
#		with open(f, "rb") as fil:
#			part = MIMEApplication(
#				fil.read(),
#				Name=basename(f)
#			)
	with open(files, "rb") as fil:
		part = MIMEApplication(
			fil.read(),
			Name=basename(files)
		)
		# After the file is closed
	part['Content-Disposition'] = 'attachment; filename="%s"' % basename(files)
	mail_msg.attach(part)
        s = smtplib.SMTP('post.tau.ac.il')
        s.sendmail(mail_msg['From'], mail_msg['To'].split(",") , mail_msg.as_string())

def send_email_with_cc(to_list, cc_list, subject, email_body):

        msg = MIMEText(email_body, 'plain', 'utf-8')
	from_sender = os.popen("hostname").read()
	msg['Subject'] = subject
	msg['From'] = from_sender[:-1]
	msg['To'] = to_list
	msg['Cc'] = cc_list
        recipients = msg['To'].split(",") + msg['Cc'].split(",")

	s = smtplib.SMTP('post.tau.ac.il')
	s.sendmail(msg['From'], recipients, msg.as_string())


#-----------------------------------------------------------------------------------
#       General utility to log messages
#-----------------------------------------------------------------------------------
def log_message(message):

        #Adding time to the log file
        current_time = os.popen("date +%F:%H%M").read()
        os.system("echo " + current_time[:-1] + " - " + message + " >> /var/log/scan_log.log")

