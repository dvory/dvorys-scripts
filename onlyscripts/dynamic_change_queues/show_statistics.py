#!/usr/bin/env python
# /usr/local/python-anaconda-2.7/bin/python

import os
import re
import pickle
# Plots:
import matplotlib.pyplot as plt
from matplotlib import style
import nodes_db
import sys
import argparse

#parser = argparse.ArgumentParser()
parser = argparse.ArgumentParser(
	prog='shload',
	formatter_class=argparse.RawDescriptionHelpFormatter,
	description="********************************************************\n* shload is for displaying queues\n*\n* shload-all is for displaying two windows of all nodes\n*\n* Displays both groups 1 and 2\n********************************************************")
#      description='''\
#         additional information:
#             It is indented
#             exactly the way
#             I want it
#         ''')
parser.add_argument('--pbspro', help='Display power8 nodes', action="store_true")
parser.add_argument('--average','--ave', help='Display average load', action="store_true")
parser.add_argument('--now', help='Display current load', action="store_true")
parser.add_argument('--queue', '-q', help='Select queue to display', dest="queue_to_display")
parser.add_argument('--group', '-g', help='Select group to display 1 or 2', dest="group_to_display")
args = parser.parse_args()
style.use('ggplot')

hostname=os.popen("hostname").read()
if hostname.find("power") != -1 or hostname.find("admin") != -1:
        host = "power"
        # Adding environment variables, in order to be able run qmgr, qstat commands
        os.environ["PATH"] = os.environ["PATH"] + ":/opt/torque/bin"

else:
        host = "lecs2"
        # Adding environment variables, in order to be able run qstat, qconf commands
        os.environ["PATH"] = os.environ["PATH"] + ":/opt/gridengine/bin/linux-x64"
        os.environ["SGE_ROOT"] = "/opt/gridengine"
        os.environ["SGE_QMASTER_PORT"] = "536"

#Dvory adding --pbspro argument to be the default:
args.pbspro=True

# -------------------------------------------------------------------------------
# If info is requested now need to issue a request to the corresponding
# head node: power2 or power5
#--------------------------------------------------------------------------------
if args.now == True:
	if host == "power":
	        # Have to read the data from the relevant head node
        	hostname=os.popen("hostname").read() # check if that is me
        	if args.pbspro == True: # need to read from power5
                	if hostname.find("power5") != -1:   # Need to read from myself
                        	command = nodes_db.current_path + "/read_data pbspro"
                	else:
                                command = "ssh -q -x power5 " + nodes_db.current_path + "/read_data pbspro"
        	else:   # Need to read from power2
                	if hostname.find("power2") != -1:   # Need to read from myself
                        	command = nodes_db.current_path + "/read_data"
                	else:
                                command = "ssh -q -x power2 " + nodes_db.current_path + "/read_data"
	else:
		command = nodes_db.current_path + "/read_data"
        os.system(command)
        print "Finished reading all data"
        args.average == False

# -------------------------------------------------------------------------------
# Loading the saved file
#--------------------------------------------------------------------------------
if host == "power":
	if args.pbspro == True:
        	nodes_array = pickle.load(open (nodes_db.path_of_pbs_pro_saved_file,"r"))
		queue_split_char = ','
	else:
        	nodes_array = pickle.load(open (nodes_db.path_of_saved_file,"r"))
        	queue_split_char = None
else: # - ignore
	if args.pbspro == True:
		print "--pbspro parameter is invalid. Ignore"
		args.pbspro = False
	nodes_array = pickle.load(open (nodes_db.path_of_saved_file,"r"))
	queue_split_char = None

all_queue_names = []

# Plotting......


# ------------------------------------------------------
#	Add color legend to the plot
# A proxy legend, since we already determined the bars' colors
# so need to set it seperately
# ------------------------------------------------------
def Add_legend(axis):

	red_proxy = plt.Rectangle((0, 0), 1, 1, fc = "r")
	green_proxy = plt.Rectangle((0, 0), 1, 1, fc = "g")
	background_proxy = plt.Rectangle((0, 0), 1, 1, fc = "lemonchiffon")
	full_load_proxy = plt.Rectangle((0, 0), 1, 1, fc = "indigo")
#Put it higher:
#        axis.legend([red_proxy, green_proxy, background_proxy, full_load_proxy], ['Unavailable', 'Load', 'Free', 'Load > 100%'], bbox_to_anchor=(-0.03, 1.8))
	axis.legend([red_proxy, green_proxy, background_proxy, full_load_proxy], ['Unavailable', 'Load', 'Free', 'Load > 100%'], bbox_to_anchor=(+1.12, 1.9))

# ------------------------------------------------------
#      Write only first letter and additional letters of queue
# because of lack of space
# ------------------------------------------------------
def Get_shorter_name(name):

	if len(name) <= 0:
		return ""
	if host == "power" or len(name) < 7:
		return name[0] + name[len(name)-1] 
	else:
		return name[0] + name[3] + name[4] + name[6]


# ------------------------------------------------------
#       Add queues legend to the plot
# A proxy legend, since we already set the bars, and it
# is not per bar
# also - it is legend without colors/symbols, therefore
# with fake rectangle, as in:
# http://stackoverflow.com/questions/16826711/is-it-possible-to-add-a-string-as-a-legend-item-in-matplotlib
# ------------------------------------------------------
def Generate_legend_queue():
	legend_text = ""
	for name in all_queue_names:
		shorter = Get_shorter_name(name)
		if shorter != "":
			legend_text = legend_text + Get_shorter_name(name) + ': ' + name + '\n'
	return legend_text

# ------------------------------------------------------
#      Create legend for queue names:
# add each name to the list, if it is unique
# ------------------------------------------------------
def Add_to_legend_queues(queues_names):
	names = queues_names.split(queue_split_char)
#	print names
	for queue_name in names:
		if queue_name not in all_queue_names:
			all_queue_names.append(queue_name)
	all_queue_names.sort()

# ------------------------------------------------------
#      Write only first and last letters of queue
# because of lack of space
# ------------------------------------------------------
def short_name(name):
	if len(name) > 0:
		return name[0] + name[len(name)-1]

# ------------------------------------------------------
#      Need to build the 'tick' for the x compute node
# ------------------------------------------------------
def Prepare_label(compute_number, queues_names):
	label_to_return = str(compute_number)
	if len(queues_names) > 0:
		names = queues_names.split(queue_split_char)
		for queue_name in names:
			label_to_return = label_to_return + '\n' + Get_shorter_name(queue_name)
	return label_to_return

# ------------------------------------------------------
#	Plot some compute nodes
# with help from https://www.quora.com/What-is-a-simple-way-to-create-a-stacked-multi-clustered-bar-plot-in-Pythons-matplotlib
# ------------------------------------------------------
def Create_subplot(min_index, max_index, axis, p, show_legend):

	ind = range(min_index,max_index)
	queues_names = []
	load = []
	idle = []
	my_colors = []
	labels = []	# The x ticks names
	for i in range(min_index, max_index):
		load.append(nodes_array[i].Get_my_percentage_load(args.average))
		idle.append(float(100))
#		print nodes_array[i].Get_my_queues()
		queues_names.append(nodes_array[i].Get_my_queues())
		Add_to_legend_queues(queues_names[len(queues_names)-1])
		if host == "power":
			labels.append(Prepare_label(i, queues_names[len(queues_names)-1]))
		else:
			node_number = nodes_array[i].My_last_name()
                        labels.append(Prepare_label(node_number, queues_names[len(queues_names)-1]))
		my_colors.append(nodes_array[i].My_color(args.average))
	if show_legend == True:
		Add_legend(axis)
	axis.set_xticklabels(labels)
	p.extend(axis.bar (ind, idle, color = 'lemonchiffon', width=0.5))
	p.extend(axis.bar (ind, load, color = my_colors, width=0.5))

# ------------------------------------------------------
#	Prepare the graph
# To add a subplot change the following:
# n_subplots - add 1, limits - change them, legend_colors-add False
# ------------------------------------------------------
def Print_data(nNodes, group_to_display):
	title = "Compute nodes load for " + hostname

	if host == "power":
		# ~270 nodes
#		n_subplots = 5
		n_subplots = 3
#                limits = [0, 80, 130, 180, 220, nNodes]
		if group_to_display == 1:
			limits = [0, 70, 110, 150]
		else:
			limits = [150, 200, 250, nNodes]
#		limits = [0, 80, 130, 180, 220, 250, nNodes]
#                legend_colors = [True, False, False, False, False, False]
		legend_colors = [True, False, False]
	else:
		n_subplots = 2
                limits = [0, 30, nNodes]
		legend_colors = [True, False] 
	f, ax = plt.subplots(n_subplots, 1)

	plt.suptitle(title, fontsize=20, fontweight='bold')

# Below is the size of gap between each two subgraphs:
#	plt.subplots_adjust(hspace=0.6)
        plt.subplots_adjust(hspace=1.1)

        plt.ylabel('Load %')
	plt.xlabel('Compute nodes')

        p = []  # List of bar properties

	for i in range (0, n_subplots):
		ax[i].xaxis.set_ticks(range(limits[i], limits[i+1]))
		ax[i].set_ylim(0,100)
		Create_subplot(limits[i], limits[i+1], ax[i], p, legend_colors[i])		
        ax[1].set_ylabel("Load")
	fake = plt.Rectangle((0, 0), 1, 1, fc="w", fill=False, edgecolor='none', linewidth=0)
# Put it higher
	ax[1].legend([fake],[Generate_legend_queue()], bbox_to_anchor=(-0.02, 3.5))

# ------------------------------------------------------
#	Prepare the graph per queue
# ------------------------------------------------------
def Print_data_for_queue(queue_name, nElements):
	f, ax = plt.subplots(1, 1)
	title_of_plot = "Compute nodes load for queue " + queue_name
	plt.suptitle(title_of_plot, fontsize=20, fontweight='bold')
	plt.ylabel('Load %')
	plt.xlabel('Compute nodes')
	ax.set_ylim(0,100)
	p = []  # List of bar properties
	nodes = []
	loads = []
	idle = []
	colors = []
	labels = []
	# Fill the array with compute nodes
	for i in range(0, nElements):
		queues = nodes_array[i].Get_my_queues()
		if queues.find(queue_name) != -1:
			# This node belongs to this queue
			nodes.append(i)
			loads.append(nodes_array[i].Get_my_percentage_load(args.average))
			idle.append(float(100))
			colors.append (nodes_array[i].My_color(args.average))
			if host != "power":
				labels.append(nodes_array[i].My_last_name())
	if len(nodes) == 0:
		print "\n******\nQueue " + queue_name + " does not contain nodes\n******"
		sys.exit(1)

        ax.set_xlim(0, len(nodes))

	Add_legend(ax)

	# Add load colors to the bars:
	p.extend(ax.bar(range(len(nodes)), idle, color = 'lemonchiffon'))
        p.extend(ax.bar(range(len(nodes)), loads, color = colors))

	# Make sure that the nodes numbers appear as labels for the bars:
	x_pos = [i for i, _ in enumerate(nodes)]
	plt.xticks(x_pos, nodes)

# -------------------------------------------------------------------------------
# Main function: display the information
#--------------------------------------------------------------------------------

if args.queue_to_display:
	#Displaying compute nodes per queue
	Print_data_for_queue(args.queue_to_display, nodes_array[0].Get_num_elements())
else:
	group = 1
	# groups is for 1st 150 nodes or 2nd 150 nodes
	if args.group_to_display:
		if args.group_to_display == '2':
			group = 2
#	print "group is " + str(group)
	#Displaying all compute nodes
#	print "Num of elements is " + str(nodes_array[0].Get_num_elements())
	Print_data(nodes_array[0].Get_num_elements(), group)

# Maximize plot graph in the window
#plt.tight_layout()

# Determining the size
gcf = plt.gcf()
default_size = gcf.get_size_inches()
gcf.set_size_inches(default_size[0]*3, default_size[1]*2, forward=True)
plt.show()
