#!/usr/bin/env python

import os
import datetime
import time
import re
import pickle
import nodes_db
import sys
import utils

mul_power = []
mul_lecs2 = []

# num of elements is +1 since array starts from 0, and the index (in power) is the compute node number
# so number of elements is actually 196

host=os.popen("hostname").read()
#if this is one of the power head nodes:
if host.find("power") != -1 or host.find("admin") != -1:
        host = "power"
        mul = mul_power
        # Adding environment variables, in order to be able run qmgr, qstat commands
        os.environ["PATH"] = os.environ["PATH"] + ":/opt/torque/bin"

else:
        host = "lecs2"
        mul = mul_lecs2
        # Adding environment variables, in order to be able run qstat, qconf commands
        os.environ["PATH"] = os.environ["PATH"] + ":/opt/gridengine/bin/linux-x64"
        os.environ["SGE_ROOT"] = "/opt/gridengine"
        os.environ["SGE_QMASTER_PORT"] = "536"

nodes_array = []
old_nodes_array = []	# to be filled from last DB, to check if there were changes

utils.Get_number_of_nodes()

# parameters
params = list(sys.argv)

pbs_pro = False
if len(sys.argv) == 2:
	if params[1] == "pbspro":
        	pbs_pro = True

if pbs_pro == True:
	path_of_db_file = nodes_db.path_of_pbs_pro_saved_file
else:
	path_of_db_file = nodes_db.path_of_saved_file


# -------------------------------------------------------
#       Reads all compute nodes from the 'nodes' file
#       Filling the compute nodes data base in nodes_db
# -------------------------------------------------------
def Fill_SGE_queues():
	queues = os.popen("qconf -sql").readlines()
	for queue in queues:
		queue = queue[:-1]	#queue is saved with '\n'
		nodes_array_per_queue = utils.Get_SGE_nodes_for_queue(queue)
		for queue_node in nodes_array_per_queue:
			for index in range(0, nodes_array[0].Get_num_elements()):
                                if nodes_array[index].IsExist() == True:
                                        if nodes_array[index].My_name() == queue_node:
                                                nodes_array[index].Add_queues(" " + queue)
                                                break 

# -------------------------------------------------------
#       Reads all compute nodes from the 'nodes' file
#       Filling the compute nodes data base in nodes_db
# -------------------------------------------------------
def Read_SGE_nodes_db():
        empty_array = ["", ""]
        output_lines = os.popen("qconf -sconfl 2> /dev/null").readlines()
        num_lines = len(output_lines)
        #Initialization
        empty_array = ["", ""]
        for i in range (0, num_lines):
                nodes_array.append(nodes_db.Compute_node(False, empty_array))

	index = 0
        for i in range (0, num_lines):
                compute_line = re.search(r"^compute-\d*-\d*", output_lines[i], re.I)
                if not compute_line:
                        continue
		# For each compute node fill all information
		compute_name = compute_line.group()
		command_line = "qconf -se " + compute_name + " 2> /dev/null"
		compute_node_output_lines = os.popen(command_line).read()
		nprocs = re.search(r"processors.*\d*", compute_node_output_lines, re.I)
		if not nprocs:
			continue
		nprocs_str= nprocs.group()
		procs_num = nprocs_str[len("processors            "):]

		nodes_array[index].Set_is_exist(True)
		nodes_array[index].Fill_my_name(compute_name)
		nodes_array[index].Fill_nCores(procs_num)
		index += 1

	nodes_array[0].Set_num_elements(index)
	Fill_SGE_queues()

# -------------------------------------------------------
#       Reads all compute nodes from the 'nodes' file
# 	Filling the compute nodes data base in nodes_db
# -------------------------------------------------------
def Read_nodes_file():
	file_descriptor = open(nodes_db.path_of_nodes_file, 'r')
	if file_descriptor:
		lines = file_descriptor.readlines()
		index = 0
		for line in lines:
			# This is a valid line
			tokenized_line = line.split()
			compute_num = nodes_db.Get_index(tokenized_line[0])
			if compute_num >= index:
				# todo: check it does not appear twice, and in the right order
				while compute_num > index:
					nodes_array.append(nodes_db.Compute_node(False, ""))
					index += 1
				nodes_array.append(nodes_db.Compute_node(True, tokenized_line))
			elif compute_num == -1:
				# Tokenized_line is garbage...
                                nodes_array.append(nodes_db.Compute_node(False, ""))
			index += 1
		nodes_array[0].Set_num_elements (nodes_db.GLOBAL_POWER_ELEMENTS)
		for i in range(index, nodes_array[0].Get_num_elements()):
			nodes_array.append(nodes_db.Compute_node(False, ""))

# -------------------------------------------------------
#       Reads all compute nodes from the 'pbsnodes -a'
#       Filling the compute nodes data base in nodes_db
# -------------------------------------------------------
def Fill_nodes_db():

	#Initialization
	empty_array = ["", ""]
	for i in range (0, nodes_db.GLOBAL_POWER_ELEMENTS+1):
		nodes_array.append(nodes_db.Compute_node(False, empty_array))

	# Fill real numbers
	output_lines = os.popen("pbsnodes -a").readlines()
	num_lines = len(output_lines)
	for i in range (0, num_lines):
		compute_line = re.search(r"^compute-\d*-\d*", output_lines[i], re.I)
		if not compute_line:
			continue
		for j in range(i+1, num_lines):
			line = re.search(r"^compute-\d*-\d*", output_lines[j], re.I)
			if line:
				i = j
				break
                        compute_name = compute_line.group()
			compute_index = int(compute_name[len("compute-0-"):])
			potential_queue = re.search(r"resources_available\.Qlist", output_lines[j], re.I)
			if potential_queue:
				# Assign queues to the compute node
				queues = output_lines[j]
				#print "---" + queues[len("resources_available.Qlist")+8:-1] + "---"
				nodes_array[compute_index].Add_queues(queues[len("resources_available.Qlist")+8:-1])
			potential_cores = re.search(r"pcpus = ", output_lines[j], re.I)
			if potential_cores:
				cores_line = output_lines[j]
				nodes_array[compute_index].Set_is_exist(True)
				nodes_array[compute_index].Fill_my_name(compute_name)
				nodes_array[compute_index].Fill_nCores(cores_line[len("pcpus = ")+5:-1])
	nodes_array[0].Set_num_elements(nodes_db.GLOBAL_POWER_ELEMENTS)	

# ------------------------------------------------------
#       Copy_history
# ------------------------------------------------------
def Copy_history(new_index, old_index):
	for history_index in range (0, len(old_nodes_array[old_index].history_list)):
		nodes_array[new_index].history_list.append(old_nodes_array[old_index].history_list[history_index])
	if old_nodes_array[old_index].IsExist() == True:
		print "old index is : " + str(old_index)
        	nodes_array[new_index].average_load = old_nodes_array[old_index].average_load
        	nodes_array[new_index].normalized_load = old_nodes_array[old_index].normalized_load
        	nodes_array[new_index].normalized_average_load = old_nodes_array[old_index].normalized_average_load


# ------------------------------------------------------
#       Update_db
# Once a day - check for nodes changes
# If I found the new node in the old list - then just copy its history
# If the new node does not exist in history - do nothing - it does not have history
# No need to check whether there are nodes appearing only in history array - they will be deleted
# ------------------------------------------------------
def Update_db():
	old_db_index = 0
        for i in range (0, nodes_db.GLOBAL_POWER_ELEMENTS):
		if nodes_array[i].My_name() == old_nodes_array[old_db_index].My_name():
			# Need to update history from old to new
			Copy_history(i, old_db_index)

		else:
			# Need to search whether this node still exists in old array
			for j in range (0, nodes_db.GLOBAL_POWER_ELEMENTS):
				# If it does - just copy its history
				if nodes_array[i].My_name() == old_nodes_array[j].My_name():
					Copy_history(i, j)
		old_db_index += 1

# ------------------------------------------------------
#	Prints all compute nodes values
# ------------------------------------------------------
def Print_nodes():
	for i in nodes_array:
		i.Print_me()

# ------------------------------------------------------
#       Returns tokenized uptime for a node for SGE
# ------------------------------------------------------
def Check_SGE_uptime_in_node(node_name):

	command_line = "qconf -se " + node_name + " 2> /dev/null"
	compute_node_output_lines = os.popen(command_line).read()
	load = re.search(r"load_avg=\d*\.*\d*", compute_node_output_lines, re.I)
	if load:
		load_avg = load.group()
		load_avg = load_avg[len("load_avg="):]
		return load_avg
	return "-1"

# ------------------------------------------------------
#       Returns tokenized uptime for a node
# ------------------------------------------------------
def check_uptime_in_node(node_name):
        full_command = "ssh -q -x " + node_name + " uptime"
        command_output = os.popen(full_command)
        result = command_output.read()
        tokenized_line = result.split()
        if command_output.close(): # The node does not respond
                return "-1"
	if tokenized_line[9].find("average") == -1: # We have a number
		load = tokenized_line[9] # This is the first load parameter (last 1 minute)
	else:
		load = tokenized_line[10] # uptime is in different format between compute nodes
	return load

# ------------------------------------------------------
#      Have 4 hours passed from last update 
# According to http://stackoverflow.com/questions/37261716/how-to-date-stamp-a-pickled-object
# ------------------------------------------------------
def Is_need_update_history():

	if os.path.isfile(path_of_db_file) == False:
		return True
	threshold = datetime.timedelta(hours=4)
	filetime = os.path.getmtime(path_of_db_file)
	now = time.time()
	delta = datetime.timedelta(seconds=now-filetime)
	if delta > threshold:
		print "update"
		return True
	print "Not update"
	return False

# ------------------------------------------------------
#	Time_to_check_for_nodes_changes
# Have 24 hours passed from last update
# This function is used to check whether a new node was added
# to the nodes, or subtracted
# ------------------------------------------------------
def Time_to_check_for_nodes_changes():

#	return True
        if os.path.isfile(path_of_db_file) == False:
                return True
        threshold = datetime.timedelta(hours=24)
        filetime = os.path.getmtime(path_of_db_file)
        now = time.time()
        delta = datetime.timedelta(seconds=now-filetime)
        if delta > threshold:
                print "update"
                return True
        print "Not update"
        return False

# ------------------------------------------------------
#       Fill uptime for each node
# ------------------------------------------------------
def Fill_uptime_for_nodes():
	flag_update_history = Is_need_update_history()
	for node_index in range (0, nodes_array[0].Get_num_elements()):
		if nodes_array[node_index].IsExist() == False:
			continue
		if nodes_array[node_index].My_name() in mul:
			nodes_array[node_index].Set_is_exist(False)
			continue
		if host == "power":
			node_load = check_uptime_in_node(nodes_array[node_index].My_name())
			if node_load != "-1":
				node_load = node_load[:-1]
		else:
                        node_load = Check_SGE_uptime_in_node(nodes_array[node_index].My_name())
		if node_load == "-1":
			nodes_array[node_index].Set_is_ok(False)
		else:
			nodes_array[node_index].Set_is_ok(True)
		nodes_array[node_index].Fill_load_data(node_load)
		if flag_update_history == True:
			nodes_array[node_index].Update_history(node_load)

# ------------------------------------------------------
#      Read_to_nodes_db 
# Read from current nodes file or pbsnodes -a into nodes_db
# ------------------------------------------------------
def Read_to_nodes_db():
	if pbs_pro == True:
		Fill_nodes_db()
	elif host == "power":
		Read_nodes_file()
	else:
		Read_SGE_nodes_db()

# ------------------------------------------------------
# Reads all data from nodes file and save it
#-------------------------------------------------------
if os.path.isfile(path_of_db_file) == True:
       	# If file exists - need not to read it again, just load the nodes file

       # if Time_to_check_for_nodes_changes() == True:
	if Time_to_check_for_nodes_changes() == False:
		old_nodes_array = pickle.load(open (path_of_db_file,"r"))
		Read_to_nodes_db()	# Reads to new array
		Update_db()	# update the new db with the history of the old saved file
	else:
		nodes_array = pickle.load(open (path_of_db_file,"r"))
else:
		Read_to_nodes_db()
#Print_nodes()
Fill_uptime_for_nodes()
pickle.dump(nodes_array, open (path_of_db_file,"w"))

