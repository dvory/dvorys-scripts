#!/usr/bin/env python
#/usr/local/python-anaconda-2.7/bin/python

import os
import datetime
import time
import re
import pickle
import nodes_db
import sys


def Get_SGE_nodes_for_queue(q_name):
        acl_hosts = []
        command_line = "qconf -sq " + q_name + " 2> /dev/null"
        output_line = os.popen(command_line).read()
        low_index=output_line.find("hostlist")
        if low_index == -1:
                #This queue does not exist
                return None
        high_index = output_line.find("seq_no")
        all_hosts = output_line[low_index+9:high_index]
	acl_hosts += re.findall(r"compute-\d*-\d*",all_hosts,re.I)
	if acl_hosts:
		return acl_hosts
	#sometimes there are several groups of hosts:
	tokenized_line = all_hosts.split()
	for i in range (0, len(tokenized_line)):
		command_line = "qconf -shgrp " + tokenized_line[i] + " 2> /dev/null"
		all_hosts = os.popen(command_line).read()
		acl_hosts += re.findall(r"compute-\d*-\d*",all_hosts,re.I)
	return acl_hosts

def Get_number_of_nodes():
	all_hosts = os.popen("cat /etc/hosts").readlines()
	max_node = 0
	for line in all_hosts:
		compute_node = re.search(r"^\d*\.\d*\.\d*\.\d*\ *compute-0-\d*$", line, re.I)
		if compute_node:
			node = re.search(r"compute-0-\d*", line, re.I)
			node_number = node.group()
			node_number = node_number[len("compute-0-"):]
			print "Node number is ----" + node_number+ "------"
			if int(node_number) > max_node:
				max_node = int(node_number)
	return max_node

