#!/usr/bin/env python

#This script checks whether the user's IP is in authorized IP list
# The list file is in the format of: ip after ip (with newline inbetween)
# it ignores character '#'
import socket
import os
import sys

myip = ([l for l in ([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if (not ip.startswith("127.") and not ("172.")) ][:1], [[(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) if l][0][0])

with open ('/root/list') as fp:
	for line in fp:
		if line[0] == '#':
			continue
		if line[len(line)-1] == '\n':
			ipline = line[:-1]
		else:
			ipline = line

#		print ipline + "-----\n"
#		Check if my ip matches:
		if myip == ipline:
			fp.close()
#			return True
#			raise SystemExit, 0
			sys.exit(0)
fp.close()
#return False
#raise SystemExit, 1
sys.exit(1)
