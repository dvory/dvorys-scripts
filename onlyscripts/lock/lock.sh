#!/bin/bash
# Lock only one instance of a program by danny@post.tau.ac.il

if [ -n "$1" ] ; then

running=`ps -ef | grep $1 | grep -v grep | grep -v lock`

program="$1"

        if [ "$running" ] ; then
                echo "already running"
                exit 0
        else
		echo "not running"
#                python $program
        fi

else
echo "lock will make sure only one instance of a program is running"
echo "USAGE: \"lock File\""
fi

