#!/usr/bin/python

# Reads from the standard input, checks for an error line - and sends an email if found.
# prints out the line anyway - so the legato server continues its regular work

import os
import smtplib
import sys

from email.mime.text import MIMEText

params = list(sys.argv)

if len(sys.argv) == 2:
        host_client = params[1]
else:
        print "\nusage - add host parameter\nusage:"
	print "cat err | ./mail_and_error.py test\nwhile err is the standard input and test is the subject for the msg"
        exit(-1)
msg= ""

error_words=["nsuccessful", "nsynchronized", "timed out", "ommand not", "ermission denied", "unavailable", "error"]
for line in sys.stdin.readlines():
	for word in error_words:
		# found error message:
		if line.find(word) != -1:
			email_message = "echo \' Error in backup, please examine\n\' \'" + line + "\' | /bin/mail -s \'" + host_client + ": --auto msg-- backup error\' \'dvory@tauex.tau.ac.il\'"
			os.system(email_message)
	msg = msg + line

mail_msg=MIMEText(msg,'plain','utf-8')
mail_msg['Subject'] = 'backup %s' % host_client
mail_msg['From'] = 'root@legato.tau.ac.il'
mail_msg['To'] = 'hpcbackupfw@post.tau.ac.il'
s = smtplib.SMTP('post.tau.ac.il')
s.sendmail(mail_msg['From'], mail_msg['To'], mail_msg.as_string())
# if adding more recipients (separated with comma) then need the following line:
# s.sendmail(mail_msg['From'], mail_msg['To'].split(","), mail_msg.as_string())
