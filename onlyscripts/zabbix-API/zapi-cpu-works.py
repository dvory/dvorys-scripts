#!/usr/local/bin/python

# zabbix-api user has readonly permissions
import sys
import logging
from datetime import datetime
import time
from pyzabbix import ZabbixAPI
#import zabbix_api

stream = logging.StreamHandler()
#stream = logging.StreamHandler(sys.stdout)
stream.setLevel(logging.DEBUG)
log = logging.getLogger('pyzabbix')
log.addHandler(stream)
log.setLevel(logging.DEBUG)

zapi = ZabbixAPI("http://zabbix.tau.ac.il")
zapi.login("zabbix-api","j!2bvTAL8[dp")
print "Connected to Zabbix API version %s" % zapi.api_version()

# Disable SSL certificate verification
zapi.session.verify = False

hostgroup = "Power cluster"
# Get hosts in host group
hostgroup = zapi.hostgroup.get(
	{
	'filter': { 'name':hostgroup },
	'sortfield': 'name',
	'sortorder': 'ASC',
	'limit':2,
	'select_hosts':'extend'
	})
"""
z = zabbix_api.ZabbixAPI(server='http://zabbix.tau.ac.il')
z.login(user='zabbix-api', password='j!2bvTAL8[dp')
"""
item_name='system.cpu.load[,avg1]'

#for host in hostgroup[0]['hosts']:
for host in zapi.host.get(output='extend'):
	print "Hostname is: ", host['name']
	print "Host id is: ", host['hostid']

	item = zapi.item.get({
		'output':'extend',
		'hostids':host['hostid'],
		'filter':{'key_':item_name}})

	if item:

#		print item[0]['lastvalue']
		print "Item-id: ", item[0]['itemid']
#		print "Hopefully item value: ", item[0][item_name]
"""
		# Get history:
		lastvalue = zapi.history.get({
			'history': item[0]['value_type'],
			'itemids': item[0]['itemid'],
			'output': 'extend',

			# sort by timestamp:
			'sortfield':'clock',
			'sortorder':'DESC',
			# Get only the first (=newest) entry:
			'limit':1,
			})

		if lastvalue:
			lastvalue = lastvalue[0]['value']
		print "Last value: ", lastvalue
	else:
		print "No item..."

	# host is - groupid, name, flags, internal
	hostname = host['host']
	print "Host:", hostname
	print "Host-ID:", host['hostid']

# Loop through all hosts interfaces, getting only "main" interfaces of type "agent"
#for h in zapi.hostinterface.get(output=["dns","ip","hostid"],selectHosts=["host"],filter={"main":1,"type":1}):
for h in zapi.host.get(output="extended",selectHosts=["host"],filter={"main":1,"type":1}):
	print('host has id %s' % (h['itemid']))

item_id = 'item_id'

# Create a time range
time_till = time.mktime(datetime.now().timetuple())
time_from = time_till - 60 * 60 * 4  # 4 hours

# Query item's history (integer) data
#history = zapi.history.get(itemids=[item_id],
history = zapi.history.get(itemids=item_id,
	time_from=time_from,
	time_till=time_till,
	output='extend',
	limit='5000',
)

# If nothing was found, try getting it from history (float) data
if not len(history):
	print "nothing"
	history = zapi.history.get(itemids=[item_id],
	time_from=time_from,
	time_till=time_till,
	output='extend',
	limit='5000',
	history=0,
	)

# Print out each datapoint
for point in history:
	print("{0}: {1}".format(datetime.fromtimestamp(int(point['clock']))
	.strftime("%x %X"), point['value']))

# Query item's history (integer) data
history = zapi.trends.get(itemids=[item_id],
    time_from=time_from,
    time_till=time_till,
    output='extend',
    limit='5000',
)

# If nothing was found, try getting it from history (float) data
if not len(history):
    history = zapi.trends.get(itemids=[item_id],
        time_from=time_from,
        time_till=time_till,
        output='extend',
        limit='5000',
        history=0,
    )

# Print out each datapoint
for point in history:
    print("{0}: {1}".format(datetime.fromtimestamp(int(point['clock']))
    .strftime("%x %X"), point['value']))




for h in zapi.host.get(output="extend"):
	print h['hostid']
"""

