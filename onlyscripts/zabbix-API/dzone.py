import logging
import sys
 
from zabbix_api import ZabbixAPI, ZabbixAPIException
 
BOLD = "\033[1m"
RESET = "\033[0;0m"
 
class Palette:
    last_idx = 0
    colors = ["C04000", "800000", "191970", "3EB489", "FFDB58", "000080",
              "CC7722", "808000", "FF7F00", "002147", "AEC6CF", "836953",
              "CFCFC4", "77DD77", "F49AC2", "FFB347", "FFD1DC", "B39EB5",
              "FF6961", "CB99C9", "FDFD96", "FFE5B4", "D1E231", "8E4585",
              "FF5A36", "701C1C", "FF7518", "69359C", "E30B5D", "826644",
              "FF0000", "414833", "65000B", "002366", "E0115F", "B7410E",
              "FF6700", "F4C430", "FF8C69", "C2B280", "967117", "ECD540",
              "082567"]
 
    def next(self):
        self.last_idx = (self.last_idx + 1) % len(self.colors)
        return self.colors[self.last_idx]
 
class ZabbixGrapher:
 
    def __init__(self, user, passwd, log_level=logging.INFO):
 
        try:
            # I had to spec. user+psw here to use Basic http auth to be able
            # to log in even though I supply them to login below;
            # otherwise the call failed with 'Error: HTTP Error 401: Authorization Required'
            self.zapi = ZabbixAPI(
                server="https://zabbix.example.com",
                path="/api_jsonrpc.php",
                user=user, passwd=passwd,
                log_level=log_level) # or DEBUG
 
            # BEWARE: The user must have access to the Zabxi API enabled (be
            # in the Zabbix API user group)
            self.zapi.login(user, passwd)
            print "Logged in, auth: " + self.zapi.auth
        except ZabbixAPIException as e:
            msg = None
            if "" in str(e):
                msg = "Connection to Zabbix timed out, it's likely having temporary problems, retry now or later'"
            else:
                msg = "Error communicating with Zabbix. Please check your authentication, Zabbix availability. Err: " + str(e)
 
            print BOLD + "\n" + msg + RESET
            raise ZabbixAPIException, ZabbixAPIException(msg), sys.exc_info()[2]
 
    def create_graph(self,
                     graph_name="CPU Loads All Data Nodes",
                     item_descr="Processor load15",
                     item_key=None,
                     host_group="Analytics production",
                     hostname_filter_fn=lambda dns: "-analytics-prod-data" in dns and ("-analytics-prod-data01" in dns or dns >= "aewa-analytics-prod-data15"),
                     #show_legend = True - has no effect (old Z. version?)
                     ):
 
        palette = Palette()
        try:
 
            items = self.get_items(item_descr = item_descr, item_key = item_key, host_group = host_group)
 
            if not items:
                raise Exception("No items with (descr=" + str(item_descr) +
                                ", key=" + str(item_key) + ") in the group '" +
                                host_group + "' found")
 
            # Transform into a list of {'host':.., 'itemid':..} pairs,
            # filter out unwanted hosts and sort by host to have a defined order
            item_maps = self.to_host_itemid_pairs(items, hostname_filter_fn)
            item_maps = sorted(
                filter(lambda it: hostname_filter_fn(it['host']), item_maps),
                key = lambda it: it['host'])
 
            if not item_maps:
                raise Exception("All retrieved items filtered out by the filter function; orig. items: " +
                                str(item_maps))
 
            # The graph will be created on the 1st item's host:
            graph_host = item_maps[0]['host']
 
            ## CREATE GRAPH
            # See https://www.zabbix.com/documentation/2.0/manual/appendix/api/graph/definitions
            graph_items = []
 
            for idx, item in enumerate(item_maps):
                graph_items.append({
                        "itemid": item['itemid'],
                        "color": palette.next(),
                        "sortorder": idx
                        })
 
            graph = self.zapi.graph.create({
                    "gitems": graph_items,
                    "name": graph_name,
                    "width":900,
                    "height":200
                    #,"show_legend": str(int(show_legend))
                    })
 
            print "DONE. The graph %s has been created on the node %s: %s." % (graph_name, graph_host, str(graph))
        except Exception as e:
            msg = None
            if "No API access while sending" in str(e):
                msg = "The user isn't allowed to access the Zabbix API; go to Zabbix administration and enable it (f.ex. add the group API access to the user)'"
            else:
                msg = "Error communicating with Zabbix. Please check your request and whether Zabbix is available. Err: " + str(e)
 
            print BOLD + "\n" + msg + RESET
            raise type(e), type(e)(msg), sys.exc_info()[2]
 
    def get_items(self, item_descr = None, item_key = None, host_group = None):
        if not item_descr and not item_key:
            raise ValueError("Either item_key or item_descr must be provided")
 
        ## GET ITEMS to include in the graph
        # See (Zabbix 2.0 so not 100% relevant but better doc)
        # https://www.zabbix.com/documentation/2.0/manual/appendix/api/item/get
        filters = {}
        if item_descr: filters["description"] = item_descr
        if item_key: filters["key_"] = item_key
 
        return self.zapi.item.get({
                "output":"shorten",
                "filter": filters,
                "group": host_group,
                "select_hosts": "extend"
                })
 
    @staticmethod
    def to_host_itemid_pairs(items, hostname_filter_fn):
        # List of (host, itemid) pairs sorted by host
        items_by_host = []
 
        for item in items:
            itemid = item['itemid']
            dns = item['hosts'][0]['dns']
 
            if hostname_filter_fn(dns):
                items_by_host.append({"host": dns, "itemid": itemid})
 
        return items_by_host
