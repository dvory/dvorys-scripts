#include "pomp_lib.h"

struct ompregdescr omp_rd_1 = {
  "parallel", "", 0, "./omp_mm.c", 28, 28, 62, 62
};

#define POMP_DLIST_00001 shared(omp_rd_1,omp_rd_2,omp_rd_3,omp_rd_4,omp_rd_5)

struct ompregdescr omp_rd_2 = {
  "for", "", 0, "./omp_mm.c", 38, 38, 41, 41
};

struct ompregdescr omp_rd_3 = {
  "for", "", 0, "./omp_mm.c", 42, 42, 45, 45
};

struct ompregdescr omp_rd_4 = {
  "for", "", 0, "./omp_mm.c", 46, 46, 49, 49
};

struct ompregdescr omp_rd_5 = {
  "for", "", 0, "./omp_mm.c", 54, 54, 61, 61
};

