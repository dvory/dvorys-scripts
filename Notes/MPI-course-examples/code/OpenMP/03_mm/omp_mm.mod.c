#ifdef _POMP
#  undef _POMP
#endif
#define _POMP 200110

#include "./omp_mm.c.opari.inc"
#line 1 "./omp_mm.c"
/******************************************************************************
* FILE: omp_mm.c
* DESCRIPTION:  
*   OpenMp Example - Matrix Multiply - C Version
*   Demonstrates a matrix multiply using OpenMP. Threads share row iterations
*   according to a predefined chunk size.
* AUTHOR: Blaise Barney
* LAST REVISED: 06/28/05
******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#define NRA 62                                                 
#define NCA 150                                                    
#define NCB 70                                                     

int main (int argc, char *argv[]) 
{
int	tid, nthreads, i, j, k, chunk;
double	a[NRA][NCA],           /* matrix A to be multiplied */
	b[NCA][NCB],           /* matrix B to be multiplied */
	c[NRA][NCB];           /* result matrix C */

chunk = 10;                    /* set loop iteration chunk size */

/*** Spawn a parallel region explicitly scoping all variables ***/
POMP_Parallel_fork(&omp_rd_1);
#line 28 "./omp_mm.c"
#pragma omp parallel shared(a,b,c,nthreads,chunk) private(tid,i,j,k) POMP_DLIST_00001
{ POMP_Parallel_begin(&omp_rd_1);
#line 29 "./omp_mm.c"
  {
  tid = omp_get_thread_num();
  if (tid == 0)
    {
    nthreads = omp_get_num_threads();
    printf("Starting matrix multiple example with %d threads\n",nthreads);
    printf("Initializing matrices...\n");
    }
  /*** Initialize matrices ***/
POMP_For_enter(&omp_rd_2);
#line 38 "./omp_mm.c"
  #pragma omp for schedule (static, chunk)  nowait
  for (i=0; i<NRA; i++)
    for (j=0; j<NCA; j++)
      a[i][j]= i+j;
POMP_Barrier_enter(&omp_rd_2);
#pragma omp barrier
POMP_Barrier_exit(&omp_rd_2);
POMP_For_exit(&omp_rd_2);
#line 42 "./omp_mm.c"
POMP_For_enter(&omp_rd_3);
#line 42 "./omp_mm.c"
  #pragma omp for schedule (static, chunk) nowait
  for (i=0; i<NCA; i++)
    for (j=0; j<NCB; j++)
      b[i][j]= i*j;
POMP_Barrier_enter(&omp_rd_3);
#pragma omp barrier
POMP_Barrier_exit(&omp_rd_3);
POMP_For_exit(&omp_rd_3);
#line 46 "./omp_mm.c"
POMP_For_enter(&omp_rd_4);
#line 46 "./omp_mm.c"
  #pragma omp for schedule (static, chunk) nowait
  for (i=0; i<NRA; i++)
    for (j=0; j<NCB; j++)
      c[i][j]= 0;
POMP_Barrier_enter(&omp_rd_4);
#pragma omp barrier
POMP_Barrier_exit(&omp_rd_4);
POMP_For_exit(&omp_rd_4);
#line 50 "./omp_mm.c"

  /*** Do matrix multiply sharing iterations on outer loop ***/
  /*** Display who does which iterations for demonstration purposes ***/
  printf("Thread %d starting matrix multiply...\n",tid);
POMP_For_enter(&omp_rd_5);
#line 54 "./omp_mm.c"
  #pragma omp for schedule (static, chunk) nowait
  for (i=0; i<NRA; i++)    
    {
    printf("Thread=%d did row=%d\n",tid,i);
    for(j=0; j<NCB; j++)       
      for (k=0; k<NCA; k++)
        c[i][j] += a[i][k] * b[k][j];
    }
POMP_Barrier_enter(&omp_rd_5);
#pragma omp barrier
POMP_Barrier_exit(&omp_rd_5);
POMP_For_exit(&omp_rd_5);
#line 62 "./omp_mm.c"
  }
POMP_Barrier_enter(&omp_rd_1);
#pragma omp barrier
POMP_Barrier_exit(&omp_rd_1);
POMP_Parallel_end(&omp_rd_1); }
POMP_Parallel_join(&omp_rd_1);
#line 62 "./omp_mm.c"
      /*** End of parallel region ***/

/*** Print results ***/
printf("******************************************************\n");
printf("Result Matrix:\n");
for (i=0; i<NRA; i++)
  {
  for (j=0; j<NCB; j++) 
    printf("%6.2f   ", c[i][j]);
  printf("\n"); 
  }
printf("******************************************************\n");
printf ("Done.\n");

}
