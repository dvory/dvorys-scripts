#!/bin/csh -f
set x = `gau-machine`
@ minc = "524288"
if ($?GAUSS_CACHESIZE) then
  @ y = $GAUSS_CACHESIZE
else if (($x == "i386-mac64") || ($x == "i386-mac32")) then
  @ ybytes = `/usr/sbin/sysctl -n hw.l2cachesize`
  @ ncore = `/usr/sbin/sysctl -n machdep.cpu.cores_per_package`
  @ y = $ybytes / $ncore
else if(-e /proc/cpuinfo) then
  egrep -i 'model *name.*intel' /proc/cpuinfo >/dev/null
  if ($status) then
    @ nc = 1
  else
    set nc = `grep 'cpu cores' /proc/cpuinfo | uniq | tr -d "[:alpha:][:punct:]"`
    if ("$nc" == "") @ nc = 1
    endif
  set ys = `grep 'cache size' /proc/cpuinfo | uniq | tr -d "[:alpha:][:punct:]"`
  @ y = ($ys * 1024) / $nc
else
  @ y = 1024 * 1024
  endif
if ($y < $minc) @ y = $minc
if ("$1" == "") then
  echo $y 
else
  @ y = $y / 16
  echo "CacheSize=$y"
  endif
