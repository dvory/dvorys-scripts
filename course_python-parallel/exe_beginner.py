#!/usr/local/bin/python

from multiprocessing import Pool, cpu_count, current_process

def my_id(i):
    print("Hi, I'm worker %d (with %d)" % (#<put here worker id>, #<put here worker PID>))

if __name__ == "__main__":
    p = Pool(#<start correct amount of workers>)
    p.map(my_id, range(#<start correct amount of tasks>))
