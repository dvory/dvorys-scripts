#!/usr/local/bin/python

from multiprocessing import Pool, cpu_count, current_process


def my_id(i):
    print("Hi, I'm worker %d (with %d)" % (i, current_process().pid))

if __name__ == "__main__":
    p = Pool(cpu_count()-1)
    args = [N for N in range(10, 13)]
    p.map(my_id, args)
