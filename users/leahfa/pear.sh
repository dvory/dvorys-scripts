#!/bin/bash

# Running 'pear' for all directories in the path
# Putting output in another directory - each execution outputs 4 files, beggining with some index (dir_index)
hostname
dir_index=0
for dir in /a/home/cc/tree/taucc/lifesci/leahfa/myseq/Miseq126-28555830/*
do
        if [ -d "${dir}" ] ; then
                cd ${dir}/Data/Intensities/BaseCalls
                ((dir_index+=1))
                /usr/local/bin/pear -f *R1* -r *R2* -n 410 -o /a/home/cc/tree/taucc/lifesci/leahfa/myseq/michal/"$dir_index"
        fi
done

