#!/usr/local/bin/python

import sys
import os

# Extracting lines from an input file, containing 'Assembled reads ..."

params = list(sys.argv)

if len(sys.argv) == 2:
        input_file = params[1]
else:
        print "\nUsage: assembled-reads.py <run.sh.o file>"
        exit(-1)

file_input_desc = open(input_file, 'r')
output_file = input_file + ".out"
file_output_desc = open(output_file, 'w')
line = file_input_desc.readline()
while line:
        if (line.find("Assembled reads ...") != -1):
                file_output_desc.write(line)
        line = file_input_desc.readline()
file_input_desc.close()
file_output_desc.close()

